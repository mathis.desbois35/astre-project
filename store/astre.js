export default {
  state: () => ({
    astreList: []
  }),
  mutations: {
    setAstreList (state, astreList) {
      state.astreList = astreList
    }
  },
  actions: {
    async getAstres ({
      commit
    }) {
      const astreList = await this.$axios.$get('https://api.le-systeme-solaire.net/rest/bodies/')
      if (astreList.bodies.length > 0) {
        commit('setAstreList', astreList.bodies)
      }
    }
  }
}
