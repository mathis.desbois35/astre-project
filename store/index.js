import Vue from 'vue'
import Vuex from 'vuex'
import astre from './astre'
import favorite from './favorite'

Vue.use(Vuex)

new Vuex.Store({
  modules: {
    astre,
    favorite
  }
})
