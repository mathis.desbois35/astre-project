export default {
  state: () => ({
    favoriteList: []
  }),
  mutations: {
    getFavoriteList () {
      return this.state.favoriteList
    },
    addFavorite (state, favorite) {
      state.favoriteList.push(favorite)
    },
    removeFavorite (state, favorite) {
      state.favoriteList.splice(state.favoriteList.indexOf(favorite), 1);
    }
  }
}
