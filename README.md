# astre-project

## Build Setup

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev
```

Ce projet a été réalisé dans le cadre de mon année de MBA Développeur Full-Stack afin d\'appréhender NuxtJS. Le temps consacré à ce projet à été assez de seulement une journée, cela explique l\'absence de CSS.

Le but de ce site était de récuperer les donnée de l'API à cette adresse : https://api.le-systeme-solaire.net/rest/bodies/

Avec ces données, je peux afficher la liste des astres, ainsi que les informations complètes sur un astre. Je peux aussi filtrer en fonction de différents paramètres et ajouter des astres à une liste de favoris qui est accessible depuis la page d'accueil.